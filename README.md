### Hi there 👋

**I’m a CS Undergraduate in University, and I study linux and network security related knowledge.** 


- 🔭 I’m currently working at school.
- 🌱 I’m currently learning Computer Science.
- :hammer_and_pick: I'm coding with ![](https://img.shields.io/badge/C-lightgray) ![](https://img.shields.io/badge/C++-ff69b4) ![](https://img.shields.io/badge/Java-brown) ![](https://img.shields.io/badge/.NET-blue) ![](https://img.shields.io/badge/Python-orange) ![](https://img.shields.io/badge/Rust-lightgreen) ![](https://img.shields.io/badge/Vue-orange)
- 💬 Ask me about linux and network.
<!--
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
![](https://github-readme-stats.vercel.app/api?username=duan-dky&show_icons=true&theme=dark)

#### :bar_chart: [Monthly coding time](https://github.com/muety/wakapi)
<!--START_SECTION:waka-->

```text
Python         10 hrs 50 mins  🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜   59.33 %
Markdown       3 hrs 9 mins    🟩🟩🟩🟩🟨⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜   17.29 %
C++            2 hrs 33 mins   🟩🟩🟩🟨⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜   14.04 %
JSON           51 mins         🟩⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜   04.68 %
CSV            30 mins         🟨⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜   02.74 %
Text           17 mins         🟨⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜   01.61 %
```

<!--END_SECTION:waka-->
